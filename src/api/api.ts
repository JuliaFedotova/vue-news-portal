import type {News} from "@/hooks/useNews";

export const api = {
  async  getNews() {
        const response = await fetch('http://localhost:3000/news');
        const json = await response.json();

        return json;
  },

  async addNews(formData: FormData) {

     await fetch('http://localhost:3000/news', {
          method: 'post',
          body: formData,
      })
  },

    async editNews(newsId: number, formData: FormData):Promise<News[]> {
       const response = await fetch(`http://localhost:3000/news/${newsId}`, {
           method: 'put',
           body: formData,
       })
        const json = await response.json();

       return json;

    },

  async getNewsById(newsId: number):Promise<News> {
      const response = await  fetch(`http://localhost:3000/news/${newsId}`)
      const json = await response.json();

      return json;
  },

  async deleteNews(newsId: number):Promise<News[]> {
     const response =  await fetch(`http://localhost:3000/news/${newsId}`, {
          method: 'delete',
      })
      const json = await response.json();

     return json;
  }
};