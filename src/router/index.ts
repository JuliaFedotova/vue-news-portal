import { createRouter, createWebHistory } from 'vue-router';
import NewsView from '@/views/NewsView.vue';
import AddNewsView from '@/views/AddNewsView.vue';
import DetailView from '@/views/DetailView.vue';
import EditNewsView from '@/views/EditNewsView.vue';

const routes = [
  { path: '/', name: 'home', component: NewsView },
  { path: '/add', name: 'addNews', component: AddNewsView },
  { path: '/detail/:id', name: 'detail', component: DetailView },
  { path: '/edit/:id', name: 'edit', component: EditNewsView },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});
