import { ref } from 'vue';
import {api} from "@/api/api";
export interface News  {
    title: string;
    description: string;
    image: string;
    id: number;
}

const newsList = ref<News[]>([]);

export function useNews() {

   async function loadNews() {
        const news = await api.getNews();

        newsList.value = news;
   }

    return {
        newsList,
        loadNews,
    }
}