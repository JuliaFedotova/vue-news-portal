export interface NewsFormData {
    title: string;
    description: string;
    image?: File | string;
}