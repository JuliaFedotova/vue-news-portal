const express = require("express");
const cors = require('cors');
const fs = require("fs");
const multipart = require('connect-multiparty');

const multipartMiddleware = multipart();

const app = express()

app.use(cors());
app.use(express.static(__dirname + '/data'))
app.use(express.json());
app.use(express.urlencoded());
app.use(express.raw())
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
});


const port = 3000

app.get('/news/:id', (req, res) => {
    const newsListString = fs.readFileSync("./backend/data/news.json", "utf8");
    const newsList = JSON.parse(newsListString);
    const newsId = Number(req.params.id);
    const result = newsList.find(function (news) {
        if (news.id === newsId) {
            return true;
        } else {
            return false;
        }
    })

    if (result) {
        res.send({
            ...result,
            image: result.image ? `http://localhost:3000${result.image}` : '',
        });
    } else {
        res.send({})
    }
})

app.put('/news/:id', multipartMiddleware, (req, res) => {
    const newsListString = fs.readFileSync("./backend/data/news.json", "utf8");
    const newsList = JSON.parse(newsListString);
    const newsId = Number(req.params.id);

    const {title, description} = req.body;

    const index = newsList.findIndex(function (news) {
        return news.id === newsId;
    })

    if(index === -1) {
        res.sendStatus(404);
        return;
    }

    newsList[index] = {
        ...newsList[index],
        title,
        description,
    }

    if (req.files?.image) {
        newsList[index].image = saveFile(req.files.image);
    }

    fs.writeFileSync('./backend/data/news.json', JSON.stringify(newsList));

    res.send(newsList[index]);
} )

app.post('/news', multipartMiddleware, (req, res) => {
    const {title, description} = req.body;
    let image = '';

    if (req.files?.image) {
        image = saveFile(req.files.image);
    }

    const result = addNews({
        title,
        description,
        image,
    });

    res.send(result);
})

app.delete('/news/:id', multipartMiddleware, (req, res) => {
    const newsListString = fs.readFileSync("./backend/data/news.json", "utf8");
    const newsList = JSON.parse(newsListString);
    const newsId = Number(req.params.id);

    const index = newsList.findIndex(function (news) {
        if (news.id === newsId) {
            return true;
        } else {
            return false;
        }
    })

    if(index >= 0) {
        newsList.splice(index, 1);

        fs.writeFileSync('./backend/data/news.json', JSON.stringify(newsList));
    }

    res.send(newsList);

})

app.get('/news', multipartMiddleware, (req, res) => {
    const newsListString = fs.readFileSync("./backend/data/news.json", "utf8");
    const newsList = JSON.parse(newsListString);

    res.send(newsList.map(newsData => ({
        ...newsData,
        image: newsData.image ? `http://localhost:3000${newsData.image}` : ''
    })));
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})

function addNews(news) {
    const dataString = fs.readFileSync("./backend/data/news.json", "utf8");
    const newsList = JSON.parse(dataString);
    const newsAmount = newsList.length;
    const id = newsAmount > 0 ? newsList[newsAmount - 1].id + 1 : 1;
    const newObject = {
        ...news,
        id,
    }

    newsList.push(newObject);

    fs.writeFileSync('./backend/data/news.json', JSON.stringify(newsList));

    return newObject;
}

function saveFile(image: { name: string; path: string }) {

    const fileContent = fs.readFileSync(image.path);
    const path = '/images/' + image.name;

    fs.writeFileSync(__dirname + '/data' + path, fileContent);

    return path;
}


